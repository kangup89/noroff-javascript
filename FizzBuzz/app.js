let input = document.getElementById("inputNumber");
input.addEventListener("keyup", fizzbuzzNumber);

function fizzbuzzNumber(){
    let result = document.getElementById("result");

    console.log("Number is " + input.value);

    fizzbuzz(input.value, result);
}

function fizzbuzzLoop(){
    let n = document.getElementById("numberOfIteration").value;
    let list = document.getElementById("fizzbuzzList");
    while(list.firstChild){
        list.firstChild.remove();
    }

    for(i = 1; i <= n; i++){
        let li = document.createElement("li");
        list.appendChild(fizzbuzz(i, li));
    }
}

function fizzbuzz(n, el){
    if(n > 0){
        if(n%15 == 0){
            el.innerText = "fizzbuzz";
            el.style.color = "purple";
        }
        else if(n%3 == 0){
            el.innerText = "fizz";
            el.style.color = "blue";
        }else if(n%5 == 0){
            el.innerText = "buzz";
            el.style.color = "red";
        }else{
            el.innerText = n;
            el.style.color = "gray";
        }
    } 
    
    return el;
}