const playerInfo = document.getElementById("player-info");
const gameInfo = document.getElementById("game-info");

const newName = document.getElementById("playerName");
const weapons = document.getElementById("weapons");

function onNameChange() {
    document.getElementById("new-name").innerText = 'Your player name will be ' + newName.value;

    game.player.name = newName.value;
}

function acceptName() {
    document.getElementById("intro").style.display = "none";
    document.getElementById("section-player-name").style.display = "none";
    document.getElementById("game").style.display = "block";

    document.getElementById("attackBtn").style.display = 'inline';
    document.getElementById("playAgain").style.display = 'none';
    playerInfo.innerText = game.player.name;
}

function weaponChanged() {
    game.player.takeWeapon(weapons.value);
    playerInfo.innerText = game.player.name + " with a " + game.player.weapon;

    document.getElementById("choose").style.display = 'none';    
}

function attack() {
    if (weapons.value != "") {
        alert(game.player.attack());
    } else {
        alert('Please select a weapon');
    }   

    if (game.gameover) {
        document.getElementById("attackBtn").style.display = 'none';
        document.getElementById("playAgain").style.display = 'inline';
    }
}

function playAgain() {
    document.getElementById("new-name").innerText = '';
    document.getElementById("intro").style.display = "block";
    document.getElementById("section-player-name").style.display = "block";
    document.getElementById("game").style.display = "none";
    document.getElementById("choose").style.display = 'block';  
    playerInfo.innerText = "";
    newName.value = "";

    game.player.name = null;
    game.gameover = false;
    game.winner = null;

    game.player.health = 20;
    game.enemy.health = 15;

    weapons.value = "default";
}