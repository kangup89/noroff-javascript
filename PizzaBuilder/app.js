const order = {
    toppings: [],
    basePrice: 80,
    total: 80
}

const toppings = [{
        name: 'pepperoni',
        price: 20
    },{
        name: 'mushroom',
        price: 40
    }, {
        name: 'no-cheese',
        price: 0
    }, {
        name: 'pineapple',
        price: 30
    }
]

// Cache topping containers
let $pizza  = null;
let $mushrooms = null;
let $pepperonis = null;
let $pineapples = null;

$(document).ready(function () {
    // Write the best jQuery ever here

    $pizza = $('.pizza');
    $mushrooms = $('.mushrooms');
    $pepperonis = $('.pepperonis');
    $pineapples = $('.pineapples');

    $('.btn-topping').click(function(e) {
        switch(e.target.id){
            case 'pepperoni':
                checkTopping($pepperonis, 0);
                break;
            case 'mushroom':
                checkTopping($mushrooms, 1);
                break;
            case 'cheese':
                $pizza.toggleClass("no-cheese");
                if(order.toppings.includes(toppings[2])) {
                    order.toppings = order.toppings.filter(item => item!=toppings[2]);
                } else {
                    order.toppings.push(toppings[2]);
                }
                break;
            case 'pineapple':
                checkTopping($pineapples, 3);
                break;
            default:
                break;        
        }

        updateOrder();
    });

    function checkTopping(el, i){
        if(!order.toppings.includes(toppings[i])){
            addTopping(el, i);
        }else {
            removeTopping(el, i);
        }
    }

    function addTopping(el, i) {
        el.html(generateTopping(toppings[i]));
        order.toppings.push(toppings[i]);
        order.total += toppings[i].price;
    }

    function removeTopping(el, i) {
        el.html('');
        order.toppings = order.toppings.filter(item => item!=toppings[i]);
        order.total -= toppings[i].price;
    }

    function updateOrder() {
        $('#total').text(order.total + '.00');

        $('#order').html('');
        order.toppings.forEach(function(item) {
            $('#order').append(item.name + ', ');
        })
    }
});

// Automatically generate the toppings based on the name and id from the button.
function generateTopping(topping) {
    const looper = Array(10).fill(topping.name);
    return looper.map(item => `<div class="${item}"></div>`).join('');
}