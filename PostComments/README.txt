1. Added a searchbar on the posts page. 
   It can filter the posts by title, content and userId by using filter() method.

2. Instead of using filter() method, I used url parameter of 'postId' for get a list of comments.
   For example: https://jsonplaceholder.typicode.com/comments?postId=1 

3. Used bootstrap and google fonts for style the comments page.