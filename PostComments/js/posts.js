// IIFE.
const Poster = (function(){
    // Private
    let posts = [];

    // Public
    return  {
        getPosts() {
            return fetch('https://jsonplaceholder.typicode.com/posts')
                    .then(resp => resp.json());
        },
        createPostTemplate(post) {
            const postDiv = document.createElement('div');
            const postTitle = document.createElement('h4');
            const postBody = document.createElement('p');
            const postLink = document.createElement('a');

            postTitle.innerText = post.title;
            postBody.innerText = post.body;
            postLink.innerText = 'View comments';
            postLink.href = `/comments.html?post=${post.id}`;

            postDiv.appendChild(document.createElement('br'));
            postDiv.appendChild(postTitle);
            postDiv.appendChild(postBody);
            postDiv.appendChild(postLink);

            return postDiv;
        },
        getComments(id) {
            return fetch(`https://jsonplaceholder.typicode.com/comments?postId=${id}`)
                   .then(resp => resp.json());
        },
        createCommentTemplate(comment) {
            const commentDiv = document.createElement('div');
            const commentName = document.createElement('h4');
            const commentEmail = document.createElement('p');
            const commentBody = document.createElement('p');

            commentDiv.classList.add("col-4");  
            commentName.style.fontWeight = "bold";
            commentEmail.style.fontSize = "1.3em";
            
            commentName.innerText = `Name: ${comment.name}`;
            commentEmail.innerText = `Email: ${comment.email}`;
            commentBody.innerText = comment.body;
            
            commentDiv.appendChild(document.createElement('br'));
            commentDiv.appendChild(commentName);
            commentDiv.appendChild(commentEmail);
            commentDiv.appendChild(commentBody);

            return commentDiv;
        }
    };            
})();