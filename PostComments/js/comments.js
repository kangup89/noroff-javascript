async function getComments() {
    const postId = location.search.split('post=')[1];
    const elTitle = document.getElementById('title');
    const elComments = document.getElementById('comments');
    
    elTitle.innerText = `Comments for post ${postId}`;

    try {
        const response = await Poster.getComments(postId);

        response.forEach(comment => {
            elComments.appendChild(Poster.createCommentTemplate(comment));
        });
    }
    catch(e) {
        console.error(e);
    }
}

getComments();