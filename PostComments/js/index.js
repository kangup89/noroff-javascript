const elPosts = document.getElementById('posts');
let response;

async function getPosts() {
    try {
        response = await Poster.getPosts();

        response.forEach(post => {
            elPosts.appendChild(Poster.createPostTemplate(post));
        });
    }
    catch(e) {
        console.error(e);
    }
}

getPosts();

const elSearchType = document.getElementById("searchType");
const elSearchInput = document.getElementById("searchInput");

function searchChanged() {

    while(elPosts.firstChild) {
        elPosts.firstChild.remove();
    }

    let result; 

    switch(elSearchType.value){
        case 'title':
            result = response.filter(function(p) {
                return p.title.includes(elSearchInput.value, 0);
            })
            break;
        case 'userId':
            result = response.filter(function(p) {
                return p.userId == elSearchInput.value;
            })
            break;
        case 'body':
            result = response.filter(function(p) {
                return p.body.includes(elSearchInput.value);
            })
            break;    
        default:
            result = response;
            break;
    }

    result.forEach(post => {
        elPosts.appendChild(Poster.createPostTemplate(post));
    });
}